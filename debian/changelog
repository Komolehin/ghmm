ghmm (0.9~rc3-5) UNRELEASED; urgency=medium

  * Team upload.
  * Do not build-depends on atlas
    Closes: #1056673
  * Standards-Version: 4.6.2 (routine-update)
  * Avoid explicitly specifying -Wl,--as-needed linker flag.

 -- Andreas Tille <tille@debian.org>  Thu, 11 Jan 2024 17:06:32 +0100

ghmm (0.9~rc3-4) unstable; urgency=medium

  * Team upload.
  * Fix homepage
    Closes: #980934

 -- Andreas Tille <tille@debian.org>  Sun, 24 Jan 2021 20:27:28 +0100

ghmm (0.9~rc3-3) unstable; urgency=medium

  * Team upload.
  * Use 2to3 to port to Python3
    Closes: #936609
  * Use secure URI in debian/watch.
  * Set upstream metadata fields: Archive.
  * Add files missing in installation
  * Fix configure options
  * Add missing Build-Depends
  * Add Upstream-Contact
  * debian/copyright: use spaces rather than tabs to start continuation
    lines.
  * Standards-Version: 4.5.1 (routine-update)
  * debhelper-compat 13 (routine-update)
  * Add salsa-ci file (routine-update)
  * Rules-Requires-Root: no (routine-update)
  * No tab in license text (routine-update)
  * Set upstream metadata fields: Repository.
  * Drop `--enable-gsl` from d/rules (thanks for the hint to Frederic-Emmanuel
    PICCA)

 -- Andreas Tille <tille@debian.org>  Thu, 17 Dec 2020 11:29:27 +0100

ghmm (0.9~rc3-2) unstable; urgency=medium

  [ Chris Lamb ]
  * Enable reproducible build by removing absolute build path from ghmm-config
    Closes: #929791

  [ Andreas Tille ]
  * Standards-Version: 4.3.0

 -- Andreas Tille <tille@debian.org>  Sun, 02 Jun 2019 15:13:30 +0200

ghmm (0.9~rc3-1) unstable; urgency=medium

  * Team upload
  * Initial release (Closes: #899396)

 -- Andreas Tille <tille@debian.org>  Thu, 06 Dec 2018 11:57:15 +0100
